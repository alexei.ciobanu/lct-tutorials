@ECHO OFF
for %%f in (*.ipynb) do (
    echo %%~nf
    nb-clean clean "%%~nf.ipynb"
)