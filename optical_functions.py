import numpy as np

def q2w0(q, lam=1064e-9):
    '''
    Get waist size from q parameter.
    '''
    zr = np.imag(q)
    w0 = np.sqrt(zr * lam / np.pi)
    return w0

def q2w(q, lam=1064e-9):
    '''
    Get beam size from q parameter.
    '''
    w0 = q2w0(q, lam=lam)
    zr = np.imag(q)
    w = w0 * np.abs(q)/zr
    return w

def q_propag(q1, M, n1=1, n2=1):
    '''
    Propagate a q parameter through an ABCD matrix
    '''
    A,B,C,D = M.ravel()
    q2 = n2*(A*q1/n1 + B)/(C*q1/n1 + D)
    return q2

def accum_gouy_Siegman_n(q, M, n=0):
    '''
    1D accumulated Gouy phase of a mode of order n and beam parameter q through
    abcd matrix M.
    '''
    n = np.array(n)
    A,B,C,D = M.ravel()
    exp_j_psi = exp_j_psi = (A+B/np.conj(q))/np.abs(A+B/q)
    return exp_j_psi**(n+1/2)

def accum_gouy_Siegman_nm(qx, Mx, qy=None, My=None, n=0, m=0):
    '''
    2D accumulated Gouy phase of a mode of order n, m and beam parameter qx, qy 
    through abcd matrix Mx, My.
    '''
    n = np.array(n)
    m = np.array(m)
    if My is None:
        My = Mx
    if qy is None:
        qy = qx

    exp_j_psi_x = accum_gouy_Siegman_n(qx, Mx, n=n)
    exp_j_psi_y = accum_gouy_Siegman_n(qy, My, n=m)
    return exp_j_psi_x * exp_j_psi_y

def abcd_space(d, n=1):
    '''
    ABCD matrix for free space of d meters
    '''
    M = np.array([[1, d/n],[0, 1]])
    return M

def abcd_lens(p):
    '''
    ABCD matrix for a lens with focal power of p diopters
    '''
    M = np.array([[1, 0],[-p, 1]])
    return M

def abcd_mirror(R):
    '''
    ABCD matrix for a mirror with radius of curvature R
    '''
    return abcd_lens(2/R)

def q_eig(M):
    '''
    Computes both positive and negative solutions to the quadratic eigenmode equation
    -c*q**2 + (a-d)*q + b = 0
    '''
    A,B,C,D = M.flatten()
    root_term = np.lib.scimath.sqrt(4*B*C + (A-D)**2)
    q1 = ((A-D) + root_term)/(2*C)
    q2 = ((A-D) - root_term)/(2*C)
    if np.imag(q1) > 0:
        q = q1
    else:
        q = q2
    return q

def u_n_q(x, q, n=0, lam=1064e-9):
    '''
    1D HG electric field amplitude taken from Siegmann eq 16.54.
    '''

    def herm(n,x):
        c = np.zeros(n+1)
        c[-1] = 1
        return np.polynomial.hermite.hermval(x,c)

    w0 = q2w0(q, lam=lam)
    w = q2w(q, lam=lam)
    k = 2*np.pi/lam
    
    t1 = np.sqrt(np.sqrt(2/np.pi))
    t2 = np.sqrt(1.0/(2.0**n*np.math.factorial(n)*w0))
    t3 = np.sqrt(w0/w)
    norm = t1*t2*t3
    u = herm(n, np.divide.outer(np.sqrt(2)*x,w)) * np.exp(-1j*k*np.divide.outer(x**2,(2*q)))

    E = norm * u  
    return E

def u_nm_q(x, y=None, qx=1j, qy=None, n=0, m=0, lam=1064e-9):
    '''
    2D HG electric field amplitude
    '''
    if y is None:
        y = x
    if qy is None:
        qy = qx
    uy = u_n_q(y, qy, n, lam=lam)
    ux = u_n_q(x, qx, m, lam=lam)
    return np.outer(uy, ux)       