# LCT tutorials

This repository contains a contains two python files `LCT_functions.py` and `optical_functions.py`, which only contain function definitions. They can be imported into any python script as long as they are in the same directory as that script. There are also jupyter notebooks in this repository that run through a number of worked examples using these functions.