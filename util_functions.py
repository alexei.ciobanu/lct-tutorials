def inplace_print(*args, buffer_size=150):
    clear_str = ' '*buffer_size
    print_str = ''
    for arg in args:
        print_str += str(arg) + ' '
    print_str = print_str[:-1]
    print(clear_str+'\r'+print_str[:buffer_size], end='\r')
    return None